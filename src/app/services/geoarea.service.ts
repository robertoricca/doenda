import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeoareasResponse } from '../models/responses/GeoareasResponse';
import { environment } from 'src/environments/environment.prod';
import { GeoareaResponse } from '../models/responses/GeoareaResponse';

@Injectable({
  providedIn: 'root'
})
export class GeoareaService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

  constructor(private http: HttpClient) {}

  getGeoareas(): Observable<GeoareasResponse> {
    const url = `${environment.server_URL}${environment.geoarea_URL}`;
    return this.http.get<GeoareasResponse>(url, this.httpOptions);
  }

  addGeoarea(name: string, lat1: number, lng1: number, lat2: number, lng2: number, active: boolean): Observable<GeoareaResponse> {
    const url = `${environment.server_URL}${environment.geoarea_URL}`;
    const body = `name=${name}&lat1=${lat1}&lng1=${lng1}&lat2=${lat2}&lng2=${lng2}&active=${active}`;
    return this.http.post<GeoareaResponse>(url, body, this.httpOptions);
  }

  // tslint:disable-next-line: max-line-length
  updateGeoarea(element_id: number, name: string, lat1: number, lng1: number, lat2: number, lng2: number, active: boolean): Observable<GeoareaResponse> {
    const url = `${environment.server_URL}${environment.geoarea_URL}${element_id}`;
    const body = `name=${name}&lat1=${lat1}&lng1=${lng1}&lat2=${lat2}&lng2=${lng2}&active=${active}`;
    return this.http.put<GeoareaResponse>(url, body, this.httpOptions);
  }

  removeGeoarea(element_id: number) {
    const url = `${environment.server_URL}${environment.geoarea_URL}${element_id}`;
    const parameter = `element_id=${element_id}`;
    return this.http.delete(`${url}?${parameter}`, this.httpOptions);
  }
}
