import { Injectable, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AccountKit } from 'ng2-account-kit';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { AccessTokenResponse } from '../models/responses/AccessTokenResponse';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

  constructor(private http: HttpClient, private router: Router) {}

  emailLogin(): void {
    sessionStorage.clear();
    AccountKit.login(
      'EMAIL',
      {
        countryCode: '',
        emailAddress: ''
      },
    );
  }

  smsLogin() {
    sessionStorage.clear();
    AccountKit.login(
      'PHONE',
      {
        countryCode: '',
        phoneNumber: ''
      }
    ).then(response => this.loginCallback(response));
  }

  loginCallback(response) {
    if (response.status === 'PARTIALLY_AUTHENTICATED') {
      const code = response.code;
      this.getToken(code).subscribe(token => {
        sessionStorage.setItem('token', token.data.token);
        this.router.navigate(['webapp']);
      });

    } else if (response.status === 'NOT_AUTHENTICATED') {
    } else if (response.status === 'BAD_PARAMS') {
    }
  }


  getToken(code: string): Observable<AccessTokenResponse> {
    const url = `${environment.server_URL}${environment.auth_URL}createapp`;
    const body = `code=${code}`;
    return this.http.post<AccessTokenResponse>(url, body, this.httpOptions);
  }

  delete(): Observable<string> {
    const url = `${environment.server_URL}${environment.auth_URL}`;
    return this.http.delete<string>(url, this.httpOptions);
  }

  logout() {
    sessionStorage.clear();
  }
}
