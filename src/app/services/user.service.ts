import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';
import { CurrentUserResponse } from '../models/responses/CurrentUserResponse';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

  constructor(private http: HttpClient) {}

  getCurrentUser(): Observable<CurrentUserResponse> {
    const url = `${environment.server_URL}${environment.user_URL}`;
    return this.http.get<CurrentUserResponse>(url, this.httpOptions);
  }

  updateCurrentUser(name?: string, lastname?: string, image?: string): Observable<CurrentUserResponse> {
    const request = {
      user: {
        name,
        lastname,
        image
      }
    };
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    const url = `${environment.server_URL}${environment.user_URL}`;
    return this.http.put<CurrentUserResponse>(url, JSON.stringify(request), {headers});
  }

  addContact(code: string): Observable<CurrentUserResponse> {
    const url = `${environment.server_URL}${environment.user_URL}contact`;
    const body = `code=${code}`;
    return this.http.post<CurrentUserResponse>(`${url}`, body, this.httpOptions);
  }

  removeUser(): Observable<any> {
    const url = `${environment.server_URL}${environment.user_URL}`;
    return this.http.delete(url, this.httpOptions);
  }
}
