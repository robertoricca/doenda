import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { ChatsResponse } from '../models/responses/ChatsResponse';
import { ActionsResponse } from '../models/responses/ActionsResponse';
import { ChatResponse } from '../models/responses/ChatResponse';
import { ActionResponse } from '../models/responses/ActionResponse';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

  constructor(private http: HttpClient) {}

  // addChat(title: string, type: string, users: string): Observable<ChatResponse> {
  //   const url = `${environment.server_URL}${environment.chat_URL}`;
  //   const body = `title=${title}&type=${type}&users=${users}`;
  //   return this.http.post<ChatResponse>(url, body, this.httpOptions);
  // }
  addChat(title: string, type: string, users: string, image?: string, parent_id?: number, start_date?: string, end_date?: string, address?: string, lat?: number, lng?: number, alert?: string, alert_interval?: number, repeat_frequency?: number, repeat_weeks?: number, repeat_weekday?: number, repeat_monthday?: number
    ): Observable<ChatResponse> {
      const url = `${environment.server_URL}${environment.chat_URL}`;
      const body = this.createParams_addChat(title, type, users, image, parent_id, start_date, end_date, address, lat, lng, alert, alert_interval, repeat_frequency, repeat_weeks, repeat_weekday, repeat_monthday);
      return this.http.post<ChatResponse>(url, body, this.httpOptions);
    }

  getChats(room_id?: number, page?: number, pagesize?: number, filter?: string): Observable<ChatsResponse> {
    const url = `${environment.server_URL}${environment.chat_URL}`;
    const parameters = this.createParams_getChats(room_id, page, pagesize, filter);
    return this.http.get<ChatsResponse>(`${url}?${parameters}`, this.httpOptions);
  }

  getActions(room_id: number, from_message_id?: number, direction?: string, maxsize?: number): Observable<ActionsResponse> {
    const url = `${environment.server_URL}${environment.chat_URL}/actions`;
    const parameters = this.createParams_getActions(room_id, from_message_id, direction, maxsize);
    return this.http.get<ActionsResponse>(`${url}?${parameters}`, this.httpOptions);
  }

  removeUser(room_id: number, user_id: number): Observable<ChatResponse> {
    const url = `${environment.server_URL}${environment.chat_URL}/users/${room_id}/${user_id}`;
    const parameters = `room_id=${room_id}&user_id=${user_id}`;
    return this.http.delete<ChatResponse>(`${url}?${parameters}`, this.httpOptions);
  }

  addUser(room_id: number, user_id: string): Observable<ChatResponse> {
    const url = `${environment.server_URL}${environment.chat_URL}/users/${room_id}/${user_id}`;
    const parameters = `room_id=${room_id}&user_id=${user_id}`;
    return this.http.post<ChatResponse>(`${url}?${parameters}`, this.httpOptions);
  }

  changeRole(room_id: number, user_id: string, role: string): Observable<ChatResponse> {
    const url = `${environment.server_URL}${environment.chat_URL}/users/${room_id}/${user_id}`;
    const parameters = `room_id=${room_id}&user_id=${user_id}`;
    const body = `role=${role}`;
    return this.http.put<ChatResponse>(`${url}?${parameters}`, body, this.httpOptions);
  }

  setRead(id: string): Observable<ActionResponse> {
    const url = `${environment.server_URL}${environment.chat_URL}read/${id}`;
    const parameter = `ID=${id}`;
    return this.http.get<ActionResponse>(`${url}?${parameter}`, this.httpOptions);
  }

  /* UTILITIES */

  createParams_addChat(title, type, users, image?, parent_id?, start_date?, end_date?, address?, lat?, lng?, alert?, alert_interval?, repeat_frequency?, repeat_weeks?, repeat_weekday?, repeat_monthday?): string {
    const map = this.setParameters_addChat(title, type, users, image, parent_id, start_date, end_date, address, lat, lng, alert, alert_interval, repeat_frequency, repeat_weeks, repeat_weekday, repeat_monthday);
    return this.createString(map);
  }

  setParameters_addChat(title, type, users, image?, parent_id?, start_date?, end_date?, address?, lat1?, lng1?, lat2?, lng2?, alert?, alert_interval?, repeat_frequency?, repeat_weeks?, repeat_weekday?, repeat_monthday?): Map<string, string> {
    const map = new Map<string, string>();
    map.set('title', title);
    map.set('type', type);
    map.set('users', users);
    if (image) {
      map.set('image', image);
    }
    if (parent_id) {
      map.set('parent_id', parent_id.toString());
    }
    if (start_date) {
      map.set('start_date', start_date);
    }
    if (end_date) {
      map.set('end_date', end_date);
    }
    if (address) {
      map.set('address', address);
    }
    if (lat1) {
      map.set('lat1', lat1.toString());
    }
    if (lng1) {
      map.set('lng1', lng1.toString());
    }
    if (lat2) {
      map.set('lat2', lat2.toString());
    }
    if (lng2) {
      map.set('lng2', lng2.toString());
    }
    if (alert) {
      map.set('alert', String(alert));
    }
    if (alert_interval) {
      map.set('alert_interval', alert_interval.toString());
    }
    if (repeat_frequency) {
      map.set('repeat_frequency', repeat_frequency.toString());
    }
    if (repeat_weeks) {
      map.set('repeat_weeks', repeat_weeks.toString());
    }
    if (repeat_weekday) {
      map.set('repeat_weekday', repeat_weekday.toString());
    }
    if (repeat_monthday) {
      map.set('repeat_monthday', repeat_monthday.toString());
    }
    return map;
  }

  createParams_getChats(room_id?, page?, pagesize?, filter?): string {
    const map = this.setParams_getChats(room_id, page, pagesize, filter);
    return this.createString(map);
  }

  setParams_getChats(room_id?, page?, pagesize?, filter?): Map<string, string> {
    const map = new Map<string, string>();
    if (room_id) {
      map.set('room_id', room_id.toString());
    }
    if (page) {
      map.set('page', page.toString());
    }
    if (pagesize) {
      map.set('pagesize', pagesize.toString());
    }
    if (filter) {
      map.set('filter', filter.toString());
    }
    return map;
  }

  createParams_getActions(room_id, from_message_id?, direction?, maxsize?): string {
    const map = this.setParams_getMessages(room_id, from_message_id, direction, maxsize);
    return this.createString(map);
  }

  setParams_getMessages(room_id, from_message_id?, direction?, maxsize?): Map<string, string> {
    const map = new Map<string, string>();
    map.set('room_id', room_id.toString());
    if (from_message_id) {
      map.set('from_message_id', from_message_id.toString());
    }
    if (direction) {
      map.set('direction', direction.toString());
    }
    if (maxsize) {
      map.set('maxsize', maxsize.toString());
    }
    return map;
  }

  createString(map: Map<string, string>): string {
    let parameters = '';
    let mapLength = map.size;
    map.forEach((value: string, key: string) => {
      parameters = parameters.concat(`${key}=${value}`);
      mapLength = mapLength - 1;
      if (mapLength !== 0) {
        parameters = parameters.concat('&');
      }
    });
    return parameters;
  }
}
