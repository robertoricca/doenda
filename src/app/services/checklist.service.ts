import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ChecklistResponse } from '../models/responses/ChecklistResponse';

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

  constructor(private http: HttpClient) { }

  getChecklist(todo_id: number): Observable<ChecklistResponse> {
    const url = `${environment.server_URL}${environment.checklist_URL}${todo_id}`;
    const parameter = `todo_id=${todo_id}`;
    return this.http.get<ChecklistResponse>(`${url}?${parameter}`, this.httpOptions);
  }

  addChecklistElement(todo_id: string, title: string) {
    const url = `${environment.server_URL}${environment.checklist_URL}`;
    const body = `todo_id=${todo_id}&title=${title}`;
    return this.http.post<ChecklistResponse>(url, body, this.httpOptions);
  }

  updateChecklistElement(element_id: number, title: string, done: string, position: number): Observable<ChecklistResponse> {
    const url = `${environment.server_URL}${environment.checklist_URL}${element_id.toString()}`;
    const body = `element_id=${element_id}&title=${title}&done=${done}&position=${position}`;
    return this.http.put<ChecklistResponse>(url, body, this.httpOptions);
  }

  removeChecklistElement(element_id: number) {
    const url = `${environment.server_URL}${environment.checklist_URL}${element_id.toString()}`;
    const parameter = `element_id=${element_id}`;
    return this.http.delete(`${url}?${parameter}`, this.httpOptions);
  }
}
