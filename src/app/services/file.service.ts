import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { FileResponse } from '../models/responses/FileResponse';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private httpOptions = {
    headers: new HttpHeaders({
      enctype: 'multipart/form-data'
    })
  };

  constructor(private http: HttpClient) {}

  upload(file: FormData): Observable<FileResponse> {
    const url = `${environment.server_URL}${environment.file_URL}upload/`;
    return this.http.post<FileResponse>(url, file, this.httpOptions);
  }

  getInformation(file_id: string): Observable<FileResponse> {
    const url = `${environment.server_URL}/api/v1/filesinfo/${file_id}`;
    const parameter = `file_id=${file_id}`;
    return this.http.get<FileResponse>(`${url}?${parameter}`, this.httpOptions);
  }

  download(file_id: string) {
    const url = `${environment.server_URL}${environment.file_URL}download/${file_id}`;
    return this.http.get(url, { responseType: 'blob' });
  }
}
