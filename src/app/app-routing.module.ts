import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { WebappComponent } from './components/webapp/webapp.component';
import { AuthGuard } from './guards/auth.guard';
import { TodoDetailComponent } from './components/todo-detail/todo-detail.component';
import { ChatDetailComponent } from './components/chat-detail/chat-detail.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent},
  { path: 'home', component: HomeComponent},
  { path: 'webapp', component: WebappComponent, canActivate: [AuthGuard] },
  { path: 'webapp/:chat_id',
    children: [
      {
        path: ':todo_id',
        component: TodoDetailComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '',
        component: ChatDetailComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
