import { Geoarea } from '../classes/Geoarea';

export class GeoareaResponse {
    status: string;
    message: string;
    data: Geoarea;
}
