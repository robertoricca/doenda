import { ChecklistElement } from '../classes/ChecklistElement';

export class ChecklistResponse {
    status: string;
    message: string;
    data: {
        elements: ChecklistElement[];
        total: number;
        pagesize: number;
        page: number;
    };
}
