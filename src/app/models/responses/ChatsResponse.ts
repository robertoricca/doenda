import { Chat } from '../classes/Chat';

export interface ChatsResponse {
    status: boolean;
    message: string;
    data: {
        elements: Chat[];
        total: number;
        pagesize: number;
        page: number;
    };
}
