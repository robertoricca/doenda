export class AccessTokenResponse {
    status: boolean;
    message: string;
    data: {
        token: string;
    };
}
