import { Geoarea } from '../classes/Geoarea';

export class GeoareasResponse {
    status: string;
    message: string;
    data: {
        elements: Geoarea[];
        total: number;
        page: number;
    };
}