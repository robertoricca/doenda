import { CurrentUser } from '../classes/CurrentUser';

export class CurrentUserResponse {
    status: boolean;
    message: string;
    data: CurrentUser;
}
