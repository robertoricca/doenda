import { Action } from '../classes/Action';

export class ActionsResponse {
    status: boolean;
    message: string;
    data: {
        elements: Action[];
        total: number;
        pagesize: number;
        page: number;
    };
}
