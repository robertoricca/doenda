import { Chat } from '../classes/Chat';

export class ChatResponse {
    status: boolean;
    message: string;
    data: Chat;
}
