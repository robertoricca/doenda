export class FileResponse {
    status: boolean;
    message: string;
    data: {
        date: string;
        file_id: string;
        name: string;
        type: string;
        size: number;
        id: number;
    };
}
