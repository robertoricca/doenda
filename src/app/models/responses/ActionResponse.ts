import { Action } from '../classes/Action';

export class ActionResponse {
    status: boolean;
    message: string;
    data: Action;
}