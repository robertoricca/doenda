export class NewTodo {
    title: string;
    type: string;
    image?: string;
    users: string;
    parent_id: string;
    start_date?: string;
    end_date?: string;
    address?: string;
    lat?: string;
    lng?: string;
    alert?: string;
    alert_interval?: string;
    repeat_frequency?: string;
    repeat_weeks?: string;
    repeat_weekday?: string;
    repeat_monthday?: string;

}
