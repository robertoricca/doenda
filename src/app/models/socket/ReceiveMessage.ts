import { Action } from '../classes/Action';

export class ReceiveMessage {
    command: string;
    action: Action;
}
