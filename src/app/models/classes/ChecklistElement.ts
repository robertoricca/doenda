export class ChecklistElement {
    id: string;
    todo_id: string;
    title: string;
    done: string;
    done_date: string;
    position: string;
}
