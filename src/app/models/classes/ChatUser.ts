export class ChatUser {
    id: string;
    name: string;
    lastname: string;
    imageURI: string;
    lastRead: string;
    role: string;
}
