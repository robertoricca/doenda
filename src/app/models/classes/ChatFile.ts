export class ChatFile {
    id: string;
    date: string;
    file_id: string;
    name: string;
    type: string;
    size: string;
}
