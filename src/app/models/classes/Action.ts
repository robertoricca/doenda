export class Action {
    id: string;
    user_id: string;
    date: string;
    activity_id: string;
    room_id: string;
    status: string;
    type: string;
    text: string;
    address: string;
    lat: string;
    lng: string;
    todo_id: string;
    other_user_id: string;
    geoarea_id: string;
    attachment_file_id: string;
}
