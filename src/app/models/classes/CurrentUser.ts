import { Contact } from './Contact';

export class CurrentUser {
    id: string;
    name: string;
    lastname: string;
    image: string;
    registration_date: string;
    contacts: Contact[];
}
