import { Action } from './Action';
import { ChatFile } from './ChatFile';
import { ChatUser } from './ChatUser';

export class Chat {
    id: string;
    title: string;
    type: string;
    image: string;
    last_read_action_id: string;
    users: {
        elements: ChatUser[];
        total: number;
    };
    lastMessage: Action;
    unreadActions: number;
    linkedRooms: string[];
    tags: {
        elements: ChatUser[];
        total: number;
        page_size: number;
        page: number;
    };
    todoInfo: {
        id: string;
        room_id: string;
        status: string;
        start_date: string;
        end_date: string;
        address: string;
        lat1: string;
        lng1: string;
        lat2: string;
        lng2: string;
        alert: string;
        alert_interval: string;
        repeat_frequency: string;
        repeat_weeks: string;
        repeat_weekday: string;
        repeat_monthday: string;
    };
    files: {
        elements: ChatFile[];
        total: number;
        page_size: number;
        page: number;
    };
}
