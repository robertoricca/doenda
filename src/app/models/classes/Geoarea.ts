export class Geoarea {
    id: string;
    user_id: string;
    name: string;
    lat1: string;
    lng1: string;
    lat2: string;
    lng2: string;
    active: string;
}
