import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { Chat } from 'src/app/models/classes/Chat';
import { Router } from '@angular/router';
import { AccountKit } from 'ng2-account-kit';

@Component({
  selector: 'app-webapp',
  templateUrl: './webapp.component.html',
  styleUrls: ['./webapp.component.css']
})
export class WebappComponent implements OnInit {

  chats: Chat[];

  token: string;

  name: string;
  lastname: string;

  constructor(
    private chatService: ChatService,
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.token = sessionStorage.getItem('token');
    this.getChats();
    this.getUserID();
  }

  getChats(): void {
    this.chatService.getChats().subscribe(response => {
      this.chats = response.data.elements;
    });
  }

  getUserID(): void {
    this.userService.getCurrentUser().subscribe(response => {
      sessionStorage.setItem('id', response.data.id);
      console.log(response.data.id);
      console.log(sessionStorage.getItem('token'));
    });
  }

  logout(): void {
    this.authService.delete().subscribe();
    this.authService.logout();
    this.router.navigate(['login']);
  }

  createChat(title, type, users): void {
    this.chatService.addChat(title, type, users).subscribe(response => {
      this.chats.push(response.data);
    });
  }

  updateUser(): void {
    this.userService.updateCurrentUser(this.name, this.lastname).subscribe();
  }

  addContact(contact: string): void {
    AccountKit.login(
      'PHONE',
      {
        countryCode: '',
        phoneNumber: `${contact}`
      }
    ).then(response => this.userService.addContact(response.code));
  }

}
