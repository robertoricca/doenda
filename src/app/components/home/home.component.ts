import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
  ) {
    this.getToken();
  }

  getToken() {
    const code = this.route.snapshot.queryParams.code;
    this.authService.getToken(code).subscribe(token => {
      sessionStorage.setItem('token', token.data.token);
    });
  }

}
