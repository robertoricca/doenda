import { Component, OnInit, OnDestroy } from '@angular/core';
import { Chat } from 'src/app/models/classes/Chat';
import { ChatUser } from 'src/app/models/classes/ChatUser';
import { Action } from 'src/app/models/classes/Action';
import { ChecklistElement } from 'src/app/models/classes/ChecklistElement';
import { ActivatedRoute } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FileService } from 'src/app/services/file.service';
import { ChecklistService } from 'src/app/services/checklist.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReceiveMessage } from 'src/app/models/socket/ReceiveMessage';
import { ChatFile } from 'src/app/models/classes/ChatFile';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.css']
})
export class TodoDetailComponent implements OnInit, OnDestroy {

  socket: WebSocket;

  chat: Chat;
  firstNineUsers: ChatUser[] = [];

  messages: Action[] = [];
  specialMessages: Action[] = [];

  room_id: string;
  todo_id: string;
  user_id: string;

  form: FormGroup;
  searchFilter = '';

  checklist: ChecklistElement[];
  // checklist: ChecklistElement[] = [
  //   {
  //     id: '1',
  //     done: '0',
  //     done_date:  null,
  //     position: '1',
  //     title: 'ce1',
  //     todo_id: '1',
  //   },
  //   {
  //     id: '1',
  //     done: '0',
  //     done_date:  null,
  //     position: '1',
  //     title: 'ce1aa',
  //     todo_id: '1',
  //   },
  //   {
  //     id: '1',
  //     done: '0',
  //     done_date:  null,
  //     position: '1',
  //     title: 'ce1',
  //     todo_id: '1',
  //   },
  //   {
  //     id: '1',
  //     done: '1',
  //     done_date:  null,
  //     position: '1',
  //     title: 'ce1',
  //     todo_id: '1',
  //   },
  // ];

  constructor(
    private route: ActivatedRoute,
    private chatService: ChatService,
    private fileService: FileService,
    private checklistService: ChecklistService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.room_id = this.route.snapshot.paramMap.get('chat_id');
    this.todo_id = this.route.snapshot.paramMap.get('todo_id');
    this.user_id = sessionStorage.getItem('id');
    this.initializeSocket();
    this.getActions().then(() => {
      this.setReadLastMessage();
      this.getChat().then(() => {
        this.getFirstNineUsers();
        this.getChecklist();
      });
    });
    this.form = this.formBuilder.group({
      file: ['']
    });
  }

  ngOnDestroy() {
    this.socket.close();
  }


  /* SOCKET */

  initializeSocket(): void {
    this.socket = new WebSocket('ws://213.171.184.211:8123/');
    this.socket.onopen = () => {
      this.socket.send(JSON.stringify({
        id: Math.random().toString(36).substr(2, 11),
        command: 'HELLO',
        token: `${sessionStorage.getItem('token')}`
      }));
    };
    this.socket.onmessage = (event) => {
      if (this.isJson(event.data)) {
        const data = JSON.parse(event.data);
        if (data.command === 'RECEIVE') {
          const message: ReceiveMessage = data;
          const m: Action = message.action;
          // console.log(m);
          if (m.type === 'message' && m.user_id !== this.user_id) {
            if (m.attachment_file_id !== null || m.attachment_file_id.length > 0) {
              this.getChat();
            }
            this.messages.push(m);
          } else {
            this.specialMessages.push(m);
          }
          this.setRead(m.id);
        }
      }
    };
  }

  isJson(str: any) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
  }

  setRead(id: string) {
    return new Promise((resolve) => {
      this.chatService.setRead(id).subscribe();
      resolve();
    });
  }

  setReadLastMessage(): void {
    if (this.messages.length > 0) {
      this.chatService.setRead((this.messages[this.messages.length - 1]).id).subscribe();
    }
  }

  sendMessage(textMessage: string): void {
    const message = {
      id: Math.random().toString(36).substr(2, 11),
      command: 'SEND',
      data: {
        room_id: this.todo_id,
        text: textMessage,
        type: 'message'
      }
    };
    this.socket.send(JSON.stringify(message));
    this.addLastAction();
  }

  closeWebSocketConnection() {
    this.socket.close();
  }


  /* ACTIONS */

  getActions() {
    return new Promise((resolve) => {
      this.chatService.getActions(+this.todo_id).subscribe(response => {
        response.data.elements.forEach((value: Action) => {
          if (value.type === 'message') {
            this.messages.push(value);
          } else {
            this.specialMessages.push(value);
          }
        });
        resolve();
      });
    });
  }

  addLastAction(): void {
    this.chatService.getActions(+this.todo_id).subscribe(response => {
      const lastAction: Action = response.data.elements[response.data.elements.length - 1];
      if (lastAction.type === 'message') {
        this.messages.push(lastAction);
      } else {
        this.specialMessages.push(lastAction);
      }
    });
  }


  /* CHAT */

  getChat() {
    return new Promise((resolve) => {
      this.chatService.getChats().subscribe(response => {
        this.chat = response.data.elements.find(c => c.id === this.todo_id);
        resolve();
      });
    });
  }

  getFirstNineUsers() {
    for (let i = 0; i < 9 && i < this.chat.users.total; i++) {
      this.firstNineUsers.push(this.chat.users.elements[i]);
    }
  }

  getChecklist() {
    this.checklistService.getChecklist(+this.chat.todoInfo.id).subscribe(response => this.checklist = response.data.elements);
    // console.log(this.chat);
  }


  /* UTILITIES */

  getDate(message: Action): string {
    const year = message.date.slice(0, 4);
    const month = message.date.slice(5, 7);
    const day = message.date.slice(8, 10);
    const date = day.concat('-', month, '-', year);
    return date;
  }

  getTime(message: Action): string {
    const time = message.date.slice(11, 16);
    return time;
  }

  isNotSameDate(message: Action): boolean {
    if (this.messages.indexOf(message) === 0) {
      return true;
    } else {
      const id = this.messages.indexOf(message);
      return (this.getDate(this.messages[id - 1]) !== this.getDate(message));
    }
  }

  getChatUserImage(message: Action): string {
    return (this.chat.users.elements.find((value: ChatUser) => value.id === message.user_id)).imageURI;
  }


  /* HTML METHODS */

  openTeam() {
    document.getElementById('collegateMenu').style.display = 'none';
    document.getElementById('teamMenu').style.display = 'block';
    document.getElementById('central-box').style.opacity = '0.3';
    // document.getElementById('central-box').style.backgroundColor = 'rgba(0, 0, 0, 0.3)';
  }

  openCollegate() {
    document.getElementById('teamMenu').style.display = 'none';
    document.getElementById('collegateMenu').style.display = 'block';
    document.getElementById('central-box').style.opacity = '0.3';
    // document.getElementById('central-box').style.backgroundColor = 'rgba(0, 0, 0, 0.3)';
  }

  closeNav() {
    document.getElementById('teamMenu').style.display = 'none';
    document.getElementById('collegateMenu').style.display = 'none';
    document.getElementById('central-box').style.opacity = '1';
  }


  /* FILE METHODS */

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('file').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.form.get('file').value);
    this.completeUpload(formData).then(() => {
      this.getChat();
    });
  }

  completeUpload(formData: FormData): Promise<any> {
    return new Promise((resolve) => {
      this.fileService.upload(formData).subscribe(response => {
        const fileToUpload = {
          id: Math.random().toString(36).substr(2, 11),
          command: 'SEND',
          data: {
            text: null,
            room_id: this.room_id,
            type: 'message',
            attachment_file_id: response.data.file_id
          }
        };
        this.socket.send(JSON.stringify(fileToUpload));
        resolve();
      });
    });
  }

  downloadFile(file: ChatFile) {
    this.fileService.download(file.file_id).subscribe(response => {
      const newBlob = new Blob([response], {type: `application/${this.getFileExtension(file.name)}`});
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      }
      const data = window.URL.createObjectURL(newBlob);
      const link = document.createElement('a');
      link.href = data;
      link.download = `${this.getFileName(file.name)}.${this.getFileExtension(file.name)}`;
      link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      // tslint:disable-next-line: only-arrow-functions
      setTimeout(function() {
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    });
  }

  getFileExtension(filename: string): string {
    const dotIndex = filename.indexOf('.');
    return filename.slice(dotIndex + 1);
  }

  getFileName(filename: string): string {
    const dotIndex = filename.indexOf('.');
    return filename.slice(0, dotIndex);
  }

  getFileUserId(file_id: string) {
    const fileAction = this.messages.find(action => action.attachment_file_id === file_id);
    return fileAction.user_id;
  }

  getChatUserName(user_id: string) {
    const user = this.chat.users.elements.find(u => u.id === user_id);
    return user.name;
  }

  getFileUserName(file_id: string) {
    return this.getChatUserName(this.getFileUserId(file_id));
  }


  /* CHECKLIST */

  atLeastOneDone() {
    return this.checklist.find(checklistElement => checklistElement.done === '1');
  }

  elementCompleted(element: ChecklistElement) {
    if (element.done === '0') {
      this.checklistService.updateChecklistElement(+element.id, element.title, '1', +element.position).subscribe(response => {
        this.checklist = response.data.elements;
        console.log(this.checklist);
      });
    } else {
      this.checklistService.updateChecklistElement(+element.id, element.title, '0', +element.position).subscribe(response => {
        this.checklist = response.data.elements;
        console.log(this.checklist);
      });
    }
  }

  onDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    console.log(this.checklist);
    this.updateChecklistPositions();
  }

  updateChecklistPositions(): void {
    this.checklist.forEach((element: ChecklistElement) => {
      this.checklistService.updateChecklistElement(+element.id, element.title, element.done, this.checklist.indexOf(element)).subscribe();
    });
  }


  /* FILTERS */

  setFilter(searchText: string) {
    this.searchFilter = searchText;
  }

}
