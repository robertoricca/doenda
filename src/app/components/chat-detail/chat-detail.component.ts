import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Chat } from 'src/app/models/classes/Chat';
import { ChatUser } from 'src/app/models/classes/ChatUser';
import { Action } from 'src/app/models/classes/Action';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FileService } from 'src/app/services/file.service';
import { ReceiveMessage } from 'src/app/models/socket/ReceiveMessage';
import { ChatFile } from 'src/app/models/classes/ChatFile';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NewTodo } from 'src/app/models/forms/NewTodo';

@Component({
  selector: 'app-chat-detail',
  templateUrl: './chat-detail.component.html',
  styleUrls: ['./chat-detail.component.css']
})
export class ChatDetailComponent implements OnInit, OnDestroy {

  socket: WebSocket;

  chat: Chat;
  firstNineUsers: ChatUser[] = [];
  chats: Chat[];

  messages: Action[] = [];
  todos: Chat[] = [];
  specialMessages: Action[] = [];

  room_id: string;
  user_id: string;

  form: FormGroup;
  searchFilter = '';
  addressFilter = false;
  appointmentFilter = false;
  startFilter = false;
  progressFilter = false;
  doneFilter = false;

  closeResult: string;

  newTodo: NewTodo;
  todoCreationTitle = false;
  todoCreationDate = false;
  todoCreationLocation = false;
  todoCreationFiles = false;
  todoConfirmed = false;

  todoDate = '';
  todoStartTime = '';
  todoEndTime = '';

  newTodoFiles: ChatFile[] = [];

  openedChatUsers: Map<string, boolean>;

  searchTodoFilter = '';

  constructor(
    private route: ActivatedRoute,
    private chatService: ChatService,
    private fileService: FileService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.room_id = this.route.snapshot.paramMap.get('chat_id');
    this.user_id = sessionStorage.getItem('id');
    this.initializeSocket();
    this.getActions().then(() => {
      this.setReadLastMessage();
      this.getChat().then(() => {
        this.getFirstNineUsers();
        this.setCreateTodoChatUsers();
      });
    });
    this.form = this.formBuilder.group({
      file: ['']
    });
  }

  ngOnDestroy() {
    this.socket.close();
  }


  /* SOCKET */

  initializeSocket(): void {
    this.socket = new WebSocket('ws://213.171.184.211:8123/');
    this.socket.onopen = () => {
      this.socket.send(JSON.stringify({
        id: Math.random().toString(36).substr(2, 11),
        command: 'HELLO',
        token: `${sessionStorage.getItem('token')}`
      }));
    };
    this.socket.onmessage = (event) => {
      if (this.isJson(event.data)) {
        const data = JSON.parse(event.data);
        if (data.command === 'RECEIVE') {
          const m: Action = data.action;
          if (m.type === 'message' && m.user_id !== sessionStorage.getItem('id')) {
            if (m.attachment_file_id !== null) {
              this.getChat();
            }
            this.messages.push(m);
          } else {
            this.specialMessages.push(m);
          }
          this.setRead(m.id);
        }
      }
    };
  }

  isJson(str: any): boolean {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
  }

  setRead(id: string): Promise<any> {
    return new Promise((resolve) => {
      this.chatService.setRead(id).subscribe();
      resolve();
    });
  }

  setReadLastMessage(): void {
    if (this.messages.length > 0) {
      this.chatService.setRead((this.messages[this.messages.length - 1]).id).subscribe();
    }
  }

  sendMessage(textMessage: string): void {
    const message = {
      id: Math.random().toString(36).substr(2, 11),
      command: 'SEND',
      data: {
        room_id: this.room_id,
        text: textMessage,
        type: 'message',
        attachment_file_id: null
      }
    };
    this.socket.send(JSON.stringify(message));
    this.addLastAction();
  }

  closeWebSocketConnection() {
    this.socket.close();
  }


  /* ACTIONS */

  getActions(): Promise<any> {
    return new Promise((resolve) => {
      this.chatService.getActions(+this.room_id).subscribe(response => {
        response.data.elements.forEach((value: Action) => {
          if (value.type === 'message') {
              this.messages.push(value);
          } else {
            this.specialMessages.push(value);
          }
        });
        resolve();
      });
    });
  }

  addLastAction(): void {
    this.chatService.getActions(+this.room_id).subscribe(response => {
      const lastAction: Action = response.data.elements[response.data.elements.length - 1];
      if (lastAction.type === 'message') {
        this.messages.push(lastAction);
      } else {
        this.specialMessages.push(lastAction);
      }
    });
  }


  /* CHAT */

  getChat(): Promise<any> {
    return new Promise((resolve) => {
      this.chatService.getChats().subscribe(response => {
        this.chat = response.data.elements.find(c => c.id === this.room_id);
        this.chats = response.data.elements;
        //TODO da capire come filtrare i ToDo
        this.todos = response.data.elements.filter(c => c.type === 'todo');
        resolve();
      });
    });
  }

  getFirstNineUsers(): void {
    for (let i = 0; i < 9 && i < this.chat.users.total; i++) {
      this.firstNineUsers.push(this.chat.users.elements[i]);
    }
  }

  setCreateTodoChatUsers() {
    this.openedChatUsers = new Map<string, boolean>();
    this.chats.forEach(chat => {
      this.openedChatUsers.set(chat.id, false);
    });
  }


  /* UTILITIES */

  getDate(message: Action): string {
    const year = message.date.slice(0, 4);
    const month = message.date.slice(5, 7);
    const day = message.date.slice(8, 10);
    const date = day.concat('-', month, '-', year);
    return date;
  }

  getTime(message: Action): string {
    const time = message.date.slice(11, 16);
    return time;
  }

  isNotSameDate(message: Action): boolean {
    if (this.messages.indexOf(message) === 0) {
      return true;
    } else {
      const id = this.messages.indexOf(message);
      return (this.getDate(this.messages[id - 1]) !== this.getDate(message));
    }
  }

  getChatUserImage(message: Action): string {
    return (this.chat.users.elements.find((value: ChatUser) => value.id === message.user_id)).imageURI;
  }

  isChat(chat: Chat): boolean {
    return (chat.type === 'direct' || chat.type === 'group');
  }

  isOpened(chat_id: string) {
    return (this.openedChatUsers.get(chat_id));
  }


  /* HTML METHODS */

  openTeam() {
    document.getElementById('collegateMenu').style.display = 'none';
    document.getElementById('teamMenu').style.display = 'block';
    document.getElementById('central-box').style.opacity = '0.3';
    // document.getElementById('central-box').style.backgroundColor = 'rgba(0, 0, 0, 0.3)';
  }

  openCollegate() {
    document.getElementById('teamMenu').style.display = 'none';
    document.getElementById('collegateMenu').style.display = 'block';
    document.getElementById('central-box').style.opacity = '0.3';
    // document.getElementById('central-box').style.backgroundColor = 'rgba(0, 0, 0, 0.3)';
  }

  closeNav() {
    document.getElementById('teamMenu').style.display = 'none';
    document.getElementById('collegateMenu').style.display = 'none';
    document.getElementById('central-box').style.opacity = '1';
  }

  openDialog() {
    document.getElementById('dialog').style.display = 'block';
    document.getElementById('central-box').style.opacity = '0.3';
  }

  closeDialog() {
    document.getElementById('dialog').style.display = 'none';
    document.getElementById('central-box').style.opacity = '1';
  }


  /* FILE METHODS */

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('file').setValue(file);
    }
  }

  selectedFile(event) {
    if (event.target.files.length > 0) {
      this.newTodoFiles.push(event.target.files[0]);
      console.log(this.newTodoFiles);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.form.get('file').value);
    this.completeUpload(formData).then(() => {
      this.getChat();
    });
  }

  downloadFile(file: ChatFile) {
    this.fileService.download(file.file_id).subscribe(response => {
      const newBlob = new Blob([response], {type: `application/${this.getFileExtension(file.name)}`});
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      }
      const data = window.URL.createObjectURL(newBlob);
      const link = document.createElement('a');
      link.href = data;
      link.download = `${this.getFileName(file.name)}.${this.getFileExtension(file.name)}`;
      link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      // tslint:disable-next-line: only-arrow-functions
      setTimeout(function() {
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    });
  }

  getFileExtension(filename: string): string {
    const dotIndex = filename.indexOf('.');
    return filename.slice(dotIndex + 1);
  }

  getFileName(filename: string): string {
    const dotIndex = filename.indexOf('.');
    return filename.slice(0, dotIndex);
  }

  getFileUserId(file_id: string) {
    const files = this.messages.filter((action: Action) => action.attachment_file_id !== null);
    const fileAction = files.find(file => file.attachment_file_id === file_id);
    return fileAction.user_id;
  }

  getChatUserName(user_id: string) {
    const user = this.chat.users.elements.find(u => u.id === user_id);
    return user.name;
  }

  getFileUserName(file_id: string) {
    const user_id = this.getFileUserId(file_id);
    return this.getChatUserName(user_id);
  }

  removeNewTodoFile(file_id: string) {
    this.newTodoFiles = this.newTodoFiles.filter((file: ChatFile) => file.file_id !== file_id);
  }


  /* FILTERS */

  setFilter(searchText: string) {
    this.searchFilter = searchText;
  }

  setTodoFilter(searchText: string) {
    this.searchTodoFilter = searchText;
    console.log(this.searchTodoFilter);
  }

  updateAddressFilter() {
    if (this.addressFilter) {
      this.addressFilter = false;
      document.getElementById('addressFilter').style.backgroundColor = '#d8d8d8';
    } else {
      this.addressFilter = true;
      document.getElementById('addressFilter').style.backgroundColor = '#ff744c';
    }
  }

  updateAppointmentFilter() {
    if (this.appointmentFilter) {
      this.appointmentFilter = false;
      document.getElementById('appointmentFilter').style.backgroundColor = '#d8d8d8';
    } else {
      this.appointmentFilter = true;
      document.getElementById('appointmentFilter').style.backgroundColor = '#ff744c';
    }
  }

  updateStartFilter() {
    if (this.startFilter) {
      this.startFilter = false;
      document.getElementById('startFilter').style.backgroundColor = '#d8d8d8';
    } else {
      this.startFilter = true;
      document.getElementById('startFilter').style.backgroundColor = '#ff744c';
    }
  }

  updateProgressFilter() {
    if (this.progressFilter) {
      this.progressFilter = false;
      document.getElementById('progressFilter').style.backgroundColor = '#d8d8d8';
    } else {
      this.progressFilter = true;
      document.getElementById('progressFilter').style.backgroundColor = '#ff744c';
    }
  }

  updateDoneFilter() {
    if (this.doneFilter) {
      this.doneFilter = false;
      document.getElementById('doneFilter').style.backgroundColor = '#d8d8d8';
    } else {
      this.doneFilter = true;
      document.getElementById('doneFilter').style.backgroundColor = '#ff744c';
    }
  }


  /* TODO CREATION */

  open(todoCreation) {
    this.newTodo = new NewTodo();
    this.todoCreationTitle = true;
    this.modalService.open(todoCreation, {ariaLabelledBy: 'modal-basic-title', backdrop: 'static', keyboard: false});
  }

  todoCreation_addDate() {
    if (this.newTodo.title) {
      this.todoCreationTitle = false;
      this.todoCreationDate = true;
      this.todoCreationLocation = false;
      this.todoCreationFiles = false;
      document.getElementById('addDate').style.backgroundColor = '#ffeaad';
      document.getElementById('addLocation').style.backgroundColor = 'transparent';
      document.getElementById('addFiles').style.backgroundColor = 'transparent';
      this.checkTodoCreationButton();
    } else {
      window.alert('Devi inserire il titolo');
    }
  }

  todoCreation_addLocation() {
    if (this.newTodo.title) {
      this.todoCreationTitle = false;
      this.todoCreationDate = false;
      this.todoCreationLocation = true;
      this.todoCreationFiles = false;
      document.getElementById('addLocation').style.backgroundColor = '#ffeaad';
      document.getElementById('addDate').style.backgroundColor = 'transparent';
      document.getElementById('addFiles').style.backgroundColor = 'transparent';
      this.checkTodoCreationButton();
    } else {
      window.alert('Devi inserire il titolo');
    }
  }

  todoCreation_addFiles() {
    if (this.newTodo.title) {
      this.todoCreationTitle = false;
      this.todoCreationDate = false;
      this.todoCreationLocation = false;
      this.todoCreationFiles = true;
      document.getElementById('addFiles').style.backgroundColor = '#ffeaad';
      document.getElementById('addDate').style.backgroundColor = 'transparent';
      document.getElementById('addLocation').style.backgroundColor = 'transparent';
      this.checkTodoCreationButton();
    } else {
      window.alert('Devi inserire il titolo');
    }
  }

  checkTodoCreationButton() {

    if (this.todoDate.length >= 1) {
      document.getElementById('addDate').style.backgroundImage = 'url(assets/img/ic-appointment-active.svg)';
    } else {
      document.getElementById('addDate').style.backgroundImage = 'url(assets/img/ic-appointment-black.svg)';
    }

    if (this.newTodo.address) {
      document.getElementById('addLocation').style.backgroundImage = 'url(assets/img/ic-location-active.svg)';
    } else {
      document.getElementById('addLocation').style.backgroundImage = 'url(assets/img/ic-location-black.svg)';
    }

    if (this.newTodoFiles.length >= 1) {
      document.getElementById('addFiles').style.backgroundImage = 'url(assets/img/ic-attachment-active.svg)';
    } else {
      document.getElementById('addFiles').style.backgroundImage = 'url(assets/img/ic-attachment-black.svg)';
    }
  }

  todoCreation_confirmTodo() {
    if (this.newTodo.title) {
      this.todoCreationTitle = false;
      this.todoCreationDate = false;
      this.todoCreationLocation = false;
      this.todoCreationFiles = false;
      this.todoConfirmed = true;
      this.newTodo.users = '';
    } else {
      window.alert('Devi inserire il titolo');
    }
  }

  closeModal() {
    this.todoCreationTitle = false;
    this.todoCreationDate = false;
    this.todoCreationLocation = false;
    this.todoCreationFiles = false;
    this.todoConfirmed = false;
    this.closeChats();
    this.modalService.dismissAll();
  }

  closeChats() {
    this.openedChatUsers.forEach((value: boolean, key: string) => {
      this.openedChatUsers.set(key, false);
    });
  }

  openChatUsers(chat_id: string) {
    if (this.openedChatUsers.get(chat_id) === true) {
      this.openedChatUsers.set(chat_id, false);
    } else {
      this.openedChatUsers.set(chat_id, true);
    }
  }

  addTodoUser(user: ChatUser, chat: Chat) {
    if (!this.newTodo.users.includes(`${user.id}:`)) {
      this.newTodo.users = this.newTodo.users.concat(`${user.id}:${chat.id},`);
    } else {
      const index = this.newTodo.users.indexOf(`${user.id}:`);
      const firstPart = this.newTodo.users.substring(0, index - 1);
      const secondPart = this.newTodo.users.substring(index + chat.id.length + 2);
      // console.log('prima' + firstPart);
      // console.log('seconda' + secondPart);
      this.newTodo.users = firstPart.concat(secondPart);
    }
  }

  isAlreadyAdded(user: ChatUser) {
    return this.newTodo.users.includes(`${user.id}:`);
  }

  selectAllChatUsers(chat: Chat) {
    chat.users.elements.forEach((value: ChatUser) => this.addTodoUser(value, chat));
  }

  formatUsers() {
    if (this.newTodo.users[0] === ',') {
      this.newTodo.users = this.newTodo.users.substring(1, this.newTodo.users.length - 1);
    } else {
      this.newTodo.users = this.newTodo.users.substring(0, this.newTodo.users.length - 1);
    }
  }

  createTodo() {
    this.closeModal();

    this.newTodoFiles.forEach((value: ChatFile) => this.uploadFile(value));
    this.newTodoFiles = [];

    this.formatUsers();

    this.newTodo.type = 'todo';
    this.newTodo.parent_id = this.room_id;
    if (this.todoDate !== '') {
      this.newTodo.start_date = `${this.todoDate} ${this.todoStartTime}`;
      this.newTodo.end_date = `${this.todoDate} ${this.todoEndTime}`;
    }

    // console.log(this.newTodo.users);
    // console.log(this.chatService.createParams_addChat(this.newTodo.title, this.newTodo.type, this.newTodo.users, this.newTodo.image, this.newTodo.parent_id, this.newTodo.start_date, this.newTodo.end_date, this.newTodo.address, this.newTodo.lat, this.newTodo.lng, this.newTodo.alert, this.newTodo.alert_interval, this.newTodo.repeat_frequency, this.newTodo.repeat_weeks, this.newTodo.repeat_weekday, this.newTodo.repeat_monthday));

    this.chatService.addChat(this.newTodo.title, this.newTodo.type, this.newTodo.users, this.newTodo.image, +this.newTodo.parent_id, this.newTodo.start_date, this.newTodo.end_date, this.newTodo.address, +this.newTodo.lat, +this.newTodo.lng,  this.newTodo.alert, +this.newTodo.alert_interval, +this.newTodo.repeat_frequency, +this.newTodo.repeat_weeks, +this.newTodo.repeat_weekday, +this.newTodo.repeat_monthday).subscribe(response => this.todos.push(response.data));
  }

  uploadFile(file: ChatFile) {
    const formData = new FormData();
    this.form.get('file').setValue(file);
    formData.append('file', this.form.get('file').value);
    this.completeUpload(formData).then(() => {
      this.getChat().then(() => console.log(this.chat.files.elements));
    });
  }

  completeUpload(formData: FormData): Promise<any> {
    return new Promise((resolve) => {
      this.fileService.upload(formData).subscribe(response => {
        const fileToUpload = {
          id: Math.random().toString(36).substr(2, 11),
          command: 'SEND',
          data: {
            text: null,
            room_id: this.room_id,
            type: 'message',
            attachment_file_id: response.data.file_id
          }
        };
        this.socket.send(JSON.stringify(fileToUpload));
        resolve();
      });
    });
  }

  titleChanged() {
    if (this.newTodo.title !== null && this.newTodo.title.length >= 1) {
      document.getElementById('confirmTodo').style.backgroundColor = '#ff744c';
    } else {
      document.getElementById('confirmTodo').style.backgroundColor = '#d8d8d8';
    }
  }

}
