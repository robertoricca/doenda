import { Pipe, PipeTransform } from '@angular/core';
import { Chat } from 'src/app/models/classes/Chat';

@Pipe({
  name: 'address'
})
export class AddressPipe implements PipeTransform {

  transform(todos: Chat[], addressFilter: boolean): Chat[] {
    if (addressFilter) {
      return todos.filter(todo => todo.todoInfo.address !== null);
    }
    return todos;
  }

}
