import { Pipe, PipeTransform } from '@angular/core';
import { Chat } from 'src/app/models/classes/Chat';

@Pipe({
  name: 'start'
})
export class StartPipe implements PipeTransform {

  transform(todos: Chat[], startFilter: boolean): Chat[] {
    if (startFilter) {
      return todos.filter(todo => todo.todoInfo.status === 'todo');
    }
    return todos;
  }

}
