import { Pipe, PipeTransform } from '@angular/core';
import { ChatUser } from 'src/app/models/classes/ChatUser';
import { Chat } from 'src/app/models/classes/Chat';

@Pipe({
  name: 'userName'
})
export class UserNamePipe implements PipeTransform {

  transform(users: ChatUser[], searchText: string): ChatUser[] {
    return users.filter((user: ChatUser) => (user.name === null) || (user.name.includes(searchText)));
  }

}
