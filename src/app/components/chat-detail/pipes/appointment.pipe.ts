import { Pipe, PipeTransform } from '@angular/core';
import { Chat } from 'src/app/models/classes/Chat';

@Pipe({
  name: 'appointment'
})
export class AppointmentPipe implements PipeTransform {

  transform(todos: Chat[], appointmentFilter: boolean): Chat[] {
    if (appointmentFilter) {
      return todos.filter(todo => todo.todoInfo.start_date !== null);
    }
    return todos;
  }

}
