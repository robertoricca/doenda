import { Pipe, PipeTransform } from '@angular/core';
import { Chat } from 'src/app/models/classes/Chat';

@Pipe({
  name: 'done'
})
export class DonePipe implements PipeTransform {

  transform(todos: Chat[], doneFilter: boolean): Chat[] {
    if (doneFilter) {
      return todos.filter(todo => todo.todoInfo.status === 'done');
    }
    return todos;
  }

}
