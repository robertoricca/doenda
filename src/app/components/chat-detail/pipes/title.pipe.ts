import { Pipe, PipeTransform } from '@angular/core';
import { Chat } from 'src/app/models/classes/Chat';

@Pipe({
  name: 'title'
})
export class TitlePipe implements PipeTransform {

  transform(todos: Chat[], searchText: string): Chat[] {
    return todos.filter(todo => todo.title.includes(searchText));
  }

}
