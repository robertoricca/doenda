import { Pipe, PipeTransform } from '@angular/core';
import { ChatFile } from 'src/app/models/classes/ChatFile';

@Pipe({
  name: 'file'
})
export class FilePipe implements PipeTransform {

  transform(files: ChatFile[], searchText: string): ChatFile[] {
    return files.filter(file => file.name.includes(searchText));
  }

}
