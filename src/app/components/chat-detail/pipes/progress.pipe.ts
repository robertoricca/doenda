import { Pipe, PipeTransform } from '@angular/core';
import { Chat } from 'src/app/models/classes/Chat';

@Pipe({
  name: 'progress'
})
export class ProgressPipe implements PipeTransform {

  transform(todos: Chat[], progressFilter: boolean): Chat[] {
    if (progressFilter) {
      return todos.filter(todo => todo.todoInfo.status === 'progress');
    }
    return todos;
  }

}
