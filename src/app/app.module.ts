import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { WebappComponent } from './components/webapp/webapp.component';
import { ChatDetailComponent } from './components/chat-detail/chat-detail.component';
import { TodoDetailComponent } from './components/todo-detail/todo-detail.component';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { RequestInterceptor } from './interceptors/request.interceptor';
import { AddressPipe } from './components/chat-detail/pipes/address.pipe';
import { AppointmentPipe } from './components/chat-detail/pipes/appointment.pipe';
import { TitlePipe } from './components/chat-detail/pipes/title.pipe';
import { StartPipe } from './components/chat-detail/pipes/start.pipe';
import { ProgressPipe } from './components/chat-detail/pipes/progress.pipe';
import { DonePipe } from './components/chat-detail/pipes/done.pipe';
import { FilePipe } from './components/chat-detail/pipes/file.pipe';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { UserNamePipe } from './components/chat-detail/pipes/user-name.pipe';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    WebappComponent,
    ChatDetailComponent,
    TodoDetailComponent,
    AddressPipe,
    AppointmentPipe,
    TitlePipe,
    StartPipe,
    ProgressPipe,
    DonePipe,
    FilePipe,
    UserNamePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    MatCardModule,
    DragDropModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
