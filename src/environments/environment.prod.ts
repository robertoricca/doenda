export const environment = {
  production: true,
  server_URL: 'http://213.171.184.211',
  auth_URL: '/api/v1/sessions/',
  user_URL: '/api/v1/me/',
  chat_URL: '/api/v1/rooms/',
  file_URL: '/api/v1/files/',
  checklist_URL: '/api/v1/checklists/',
  geoarea_URL: '/api/v1/geoareas/'
};
